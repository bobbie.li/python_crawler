import requests 
import bs4
import json
import io
from multiprocessing import Pool
import time

root_url = 'http://wufazhuce.com'	

def get_url(num):
	return root_url+'/one/'+str(num)

def get_urls(num):
	urls = map(get_url,range(100,100+num))
	return urls

def get_data(url):
	datalist={}	
	response=requests.get(url)
	if(response.status_code != 200):
		return {'novalue':'novalue'}
	soup=bs4.BeautifulSoup(response.text,"html.parser")
	datalist["index"] = soup.title.string[4:7]
	for meta in soup.select('meta'):
		if meta.get('name') == 'description':
			print meta.get('content')
			datalist["content"]=meta.get('content')
	datalist['imgeURL'] = soup.find_all('img')[1]['src']
	return datalist

if __name__ == '__main__':
	pool=Pool(4)
	datalist=[]
	urls=get_urls(10)
	start=time.time()
	datalist=pool.map(get_data,urls)
	end=time.time()
	print "use %.2fs"%(end-start)
	print datalist 
	jsonData=json.dumps({'data':datalist})
	with open('data.txt','w') as outfile:
		json.dump(jsonData,outfile)
